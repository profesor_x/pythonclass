class First(object):
    def __init__(self):
        print "first before"
        super(First, self).__init__()
        print "first after"

class Second(First):
    def __init__(self):
        print "second before"
        super(Second, self).__init__()
        print "second after"

class Third(First):
    def __init__(self):
        print "third before"
        super(Third, self).__init__()
        print "third after"


class Fourth(Second, Third):
    def __init__(self):
        print "Fourth before"
        super(Fourth, self).__init__()
        print "Fourth after"

cls = Fourth()
print Fourth.__mro__
